﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Xml;

namespace ServerMonitor
{
    public partial class AddForm : Form
    {
        public AddForm()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog op = new OpenFileDialog();
            op.Filter = "Exe files (*.exe)|*.exe|All files (*.*)|*.*";
            op.Title = "Seleccione el ejecutable a Monitorear...";
            if (op.ShowDialog() == DialogResult.OK)
            {
                textBox2.Text = op.FileName;   
            }  
        }


        private void button2_Click(object sender, EventArgs e)
        {
            Form1 f1 = new Form1();
            XmlDocument doc = new XmlDocument();
            doc.Load(Form1.CfgFile);
            XmlElement f = doc.CreateElement("Application");
            XmlElement g = doc.CreateElement("AppName");
            XmlElement i = doc.CreateElement("AppPath");
            XmlElement j = doc.CreateElement("Status");
            g.InnerText = textBox1.Text;
            i.InnerText = textBox2.Text;
            j.InnerText = "U";
            f.AppendChild(g);
            f.AppendChild(i);
            f.AppendChild(j);
            doc.DocumentElement.AppendChild(f);
            doc.Save(Form1.CfgFile);
        }
    }
}
