﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Reflection;
using System.Diagnostics;
using System.Threading;
using System.Xml;

namespace ServerMonitor
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public static string CfgFile = AppDomain.CurrentDomain.BaseDirectory + Assembly.GetEntryAssembly().GetName().Name + ".xml";

        private void Form1_Load(object sender, EventArgs e)
        {
            timer1.Enabled = true;
        }



        public void IsRunning()
        {
            listView1.Items.Clear();
            ReadCfg();
            for (int i = 0; i < listView1.Items.Count; i++)
            {
                var ur = listView1.Items[i].SubItems[1].Text;
                var d = ur.LastIndexOf(@"\");
                var op = ur.Remove(0, d + 1);
                var s = op.LastIndexOf(".");
                var l = op.Length;
                var sp = op.ToString().Remove(s, 4);

                Process[] pn = Process.GetProcessesByName(sp);

                if (pn.Length == 0)
                {
                    listView1.Items[i].UseItemStyleForSubItems = false;
                    listView1.Items[i].SubItems[2].Text = "Stopped";
                    listView1.Items[i].SubItems[2].ForeColor = Color.Red;
                }
                else
                {
                    listView1.Items[i].UseItemStyleForSubItems = false;
                    listView1.Items[i].SubItems[2].Text = "Running";
                    listView1.Items[i].SubItems[2].ForeColor = Color.Green;
                }
            }
        }


        public void ReadCfg()
        {

            DataSet ds = new DataSet();

            ds.ReadXml(CfgFile);

            ListViewItem item;

            foreach (DataRow dr in ds.Tables["Application"].Rows)
            {
                item = new ListViewItem(new string[] { dr["AppName"].ToString(), dr["AppPath"].ToString(), dr["Status"].ToString() });
                listView1.Items.Add(item);
            }

        }

        private void runToolStripMenuItem_Click(object sender, EventArgs e)
        {
            try
            {

                int intselectedindex = listView1.SelectedIndices[0];
                if (intselectedindex >= 0)
                {
                    try
                    {
                        ProcessStartInfo p = new ProcessStartInfo(listView1.Items[intselectedindex].SubItems[1].Text)
                        {
                            UseShellExecute = true
                        };
                        Process.Start(p);
                        Application.EnableVisualStyles();
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.Message);
                    }
                }
            }
            catch (Exception)
            {

                throw;
            }
        }

        private void restartToolStripMenuItem_Click(object sender, EventArgs e)
        {
            int intselectedindex = listView1.SelectedIndices[0];

            if (intselectedindex >= 0)
            {
                var exeName = listView1.Items[intselectedindex].SubItems[1].Text;
                try
                {
                    var ur = exeName;
                    var d = ur.LastIndexOf(@"\");
                    var op = ur.Remove(0, d + 1);
                    var s = op.LastIndexOf(".");
                    var l = op.Length;
                    var sp = op.ToString().Remove(s, 4);

                    foreach (var pr in Process.GetProcessesByName(sp))
                    {
                        pr.Kill();
                    }

                    Thread.Sleep(2000);

                    listView1.Items[intselectedindex].SubItems[2].Text = "Running";

                    ProcessStartInfo p = new ProcessStartInfo(exeName)
                    {
                        UseShellExecute = true
                    };
                    Process.Start(p);
                    Application.EnableVisualStyles();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            IsRunning();
        }

        private void settingsToolStripMenuItem_Click(object sender, EventArgs e)
        {
            AddForm af = new AddForm();
            af.ShowDialog();
        }
    }
}
